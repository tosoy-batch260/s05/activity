<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05: Client-Server Communication (Basic To-Do)</title>
</head>

<body>
    <?php session_start() ?>

    <!-- Login Form -->
    <h1>Login</h1>
    <form action="./server.php" method="post">
        <label for="username">Username: </label>
        <input type="text" name="username" id="username" required>
        <label for="password">Password: </label>
        <input type="password" name="password" id="password" required>
        <br>
        <button type="submit">Login</button>
    </form>
    
</body>

</html>