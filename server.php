<?php

session_start();

// Check if the user has submitted login credentials.
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Verify the login credentials.
    if ($username === 'johnsmith@gmail.com' && $password === '1234') {
        // Set the session variables if the login is successful.
        $_SESSION['username'] = $username;
        $_SESSION['loggedIn'] = true;
    }
}

// Display the appropriate content based on user login status.
if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] === true) {
    // Show user information and logout button.
    echo "<h1>Welcome, " . $_SESSION['username'] . "!</h1>";
    echo "<form method='post'><button type='submit' name='logout'>Logout</button></form>";

    // Checked if the user clicked the logout button.
    if (isset($_POST['logout'])) {
        // If so, clear the user information and redirect to index.php
        $_SESSION = array();
        session_destroy();
        header("Location: index.php");
    }
}
